<?php

use App\Components\Container;
use Psr\Container\ContainerInterface;

require_once __DIR__ . '/../vendor/autoload.php';

ini_set('display_errors', 1);

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

$container = new Container();

$config = require_once('config.php');

$container->set('configs', $config);

$container->set('db', function (ContainerInterface $container) {
    $settings = $container->get('configs')['db'];

    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($settings);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
});

$db = $container->get('db');

$lavi = new \Lavi\Lavi($container);

$router = new \Lavi\Router\Router();

try {
    $router->get('/', [
        'namespace' => 'App\\Controllers',
        'controller' => 'HomeController',
        'action' => 'index'
    ]);

    $router->post('/api/auth', [
        'namespace' => 'App\\Controllers',
        'controller' => 'AuthController',
        'action' => 'auth'
    ]);

    $router->get('/api/generate', [
        'namespace' => 'App\\Controllers',
        'controller' => 'GeneratorController',
        'action' => 'generate',
        'middlewares' => [\App\Middlewares\AuthMiddleware::class]
    ]);

    $router->get('/api/retrieve', [
        'namespace' => 'App\\Controllers',
        'controller' => 'GeneratorController',
        'action' => 'retrieve',
        'middlewares' => [\App\Middlewares\AuthMiddleware::class]
    ]);

    $lavi->run($router);
} catch (\Lavi\Exceptions\LaviException $e) {
    echo $e->getMessage();
}