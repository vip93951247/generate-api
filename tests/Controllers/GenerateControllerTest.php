<?php

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class GenerateControllerTest extends TestCase
{
    const BASE_URI = 'http://localhost:8080';

    public function testGenerate()
    {
        $client = new GuzzleHttp\Client(['base_uri' => self::BASE_URI]);
        $resultAuth = $this->doAuth($client);
        $this->doGenerate($client, $resultAuth);
    }

    public function testRetrieve()
    {
        $client = new GuzzleHttp\Client(['base_uri' => self::BASE_URI]);
        $resultAuth = $this->doAuth($client);
        $resultGenerate = $this->doGenerate($client, $resultAuth);

        $response = $client->get('/api/retrieve', [
            'headers' => [
                'Authorization' => "Bearer {$resultAuth['token']}"
            ],
            'query' => [
                'id' => $resultGenerate['id']
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        $this->assertNotNull($result);
        $this->assertEquals($resultGenerate['value'], $result['value']);
    }

    private function doGenerate(Client $client, array $resultAuth)
    {
        $response = $client->get('/api/generate', [
            'headers' => [
                'Authorization' => "Bearer {$resultAuth['token']}"
            ]
        ]);

        $resultGenerate = json_decode($response->getBody()->getContents(), true);

        $this->assertNotNull($resultGenerate);
        $this->assertArrayHasKey('id', $resultGenerate);
        $this->assertArrayHasKey('value', $resultGenerate);
        $this->assertTrue($resultGenerate['value'] >= 1 &&
            $resultGenerate['value'] <= 100000);

        return $resultGenerate;
    }

    private function doAuth(Client $client)
    {
        $response = $client->post('/api/auth', [
            'form_params' => [
                'login' => 'miro',
                'pass'  => 'admin'
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotEmpty($result['token']);

        return $result;
    }
}