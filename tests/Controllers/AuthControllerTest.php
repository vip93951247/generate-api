<?php

use GuzzleHttp\Exception\ClientException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends TestCase
{
    const BASE_URI = 'http://localhost:8080';

    public function testAuthPositive()
    {
        $client = new GuzzleHttp\Client(['base_uri' => self::BASE_URI]);

        $response = $client->post('/api/auth', [
            'form_params' => [
                'login' => 'miro',
                'pass'  => 'admin'
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotEmpty($result['token']);
    }

    public function testAuthNegative()
    {
        $client = new GuzzleHttp\Client(['base_uri' => self::BASE_URI]);

        try {
            $client->post('/api/auth', [
                'form_params' => [
                    'login' => 'miro'
                ]
            ]);

            $this->expectException(ClientException::class);
        } catch (Exception $exception) {
            $this->assertEquals(Response::HTTP_BAD_REQUEST, $exception->getCode());
        }
    }
}