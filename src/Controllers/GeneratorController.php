<?php

namespace App\Controllers;

use App\Services\GeneratorService;
use Lavi\Controller\Controller;
use Lavi\Request\IRequest;
use Lavi\Response\ApiResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class GeneratorController extends Controller
{
    private GeneratorService $generatorService;

    public function __construct(IRequest $request, GeneratorService $generatorService)
    {
        parent::__construct($request);
        $this->generatorService = $generatorService;
    }

    public function generate(): ApiResponse
    {
        $response = new ApiResponse();

        $generateValue = $this->generatorService->generateValue();

        $response->setData([
            'id'    => $generateValue['hash'],
            'value' => $generateValue['value']
        ]);

        $response->send();

        return $response;
    }

    public function retrieve(): ApiResponse
    {
        $response = new ApiResponse();

        $hash = $this->request->getQueries()->get('id');

        if (!$hash) {
            $response->setContent(ApiResponse::$statusTexts[ApiResponse::HTTP_BAD_REQUEST]);
            $response->setStatusCode(ApiResponse::HTTP_BAD_REQUEST);
            $response->send();

            return $response;
        }

        $generateValue = $this->generatorService->getGenerateValueByHash($hash);

        $data = [
            'error' => 'Not found value'
        ];

        if (!empty($generateValue)) {
            $data = [
                'value' => $generateValue->value
            ];
        }

        $response->setData($data);

        $response->send();

        return $response;
    }
}