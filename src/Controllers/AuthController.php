<?php

namespace App\Controllers;

use App\Services\AuthService;
use Lavi\Controller\Controller;
use Lavi\Request\IRequest;
use Lavi\Response\ApiResponse;

class AuthController extends Controller
{
    private AuthService $authService;

    public function __construct(IRequest $request, AuthService $authService)
    {
        parent::__construct($request);
        $this->authService = $authService;
    }

    public function auth()
    {
        $login = $this->request->getRequest()->get('login');
        $pass = $this->request->getRequest()->get('pass');

        $response = new ApiResponse();

        if (!$login || !$pass) {
            $response->setContent(ApiResponse::$statusTexts[ApiResponse::HTTP_BAD_REQUEST]);
            $response->setStatusCode(ApiResponse::HTTP_BAD_REQUEST);
            $response->send();

            return $response;
        }

        $response->setData([
            'token' => $this->authService->doAuth($login, $pass)
        ]);

        $response->send();

        return $response;
    }
}