<?php

namespace App\Controllers;

use Lavi\Controller\Controller;
use Lavi\Response\ApiResponse;

class HomeController extends Controller
{
    public function index(ApiResponse $response)
    {
        $response->setData([
            'version' => 1.0
        ]);

        $response->send();

        return $response;
    }
}