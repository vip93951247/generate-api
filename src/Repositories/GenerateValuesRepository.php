<?php

namespace App\Repositories;

use Psr\Container\ContainerInterface;

class GenerateValuesRepository
{
    private $db;

    public function __construct(ContainerInterface $container)
    {
        $this->db = $container->get('db');
    }

    public function insertGenerateValue(array $data)
    {
        return $this->db->table('generate_values')->insert($data);
    }

    public function getByHash(string $hash)
    {
        return $this->db->table('generate_values')
            ->where('hash', $hash)
            ->first();
    }
}