<?php

namespace App\Repositories;

use Psr\Container\ContainerInterface;

class UserRepository
{
    private $db;

    public function __construct(ContainerInterface $container)
    {
        $this->db = $container->get('db');
    }

    public function getOneByData(string $login, string $password)
    {
        return $this->db->table('users')
            ->where('login', $login)
            ->where('password', $password)
            ->first();
    }

    public function getOneByActiveToken(string $token)
    {
        return $this->db->table('users')
            ->where('token', $token)
            ->where('token_expire_at', '>=', (new \DateTime())->format('Y-m-d H:i:s'))
            ->first();
    }

    public function createUser(array $data)
    {
        return $this->db->table('users')->insert($data);
    }

    public function updateById(array $data, int $id)
    {
        return $this->db->table('users')->where('id', $id)->update($data);
    }
}