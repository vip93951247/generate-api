<?php

namespace App\Middlewares;

use App\Services\AuthService;
use Lavi\Middleware\IMiddleware;
use Lavi\Request\IRequest;
use Lavi\Response\ApiResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthMiddleware implements IMiddleware
{
    private ContainerInterface $container;
    private AuthService $authService;

    public function __construct(ContainerInterface $container, AuthService $authService)
    {
        $this->container = $container;
        $this->authService = $authService;
    }

    public function process(IRequest $request, $handler): ResponseInterface
    {
        $badResponse = new ApiResponse(ApiResponse::$statusTexts[401], ApiResponse::HTTP_UNAUTHORIZED);

        if (!$this->hasAuthToken($request)) {
            $badResponse->send();
            return $badResponse;
        }

        $credentials = $this->getCredentials($request);

        if (!$this->authService->retrieveToken($credentials)) {
            $badResponse->send();
            return $badResponse;
        }

        return $handler->handle($request);
    }

    protected function getCredentials(IRequest $request): string
    {
        $authorizationHeader = $request->getHeaders()->get('Authorization');
        return substr($authorizationHeader, 7);
    }

    protected function hasAuthToken(IRequest $request): bool
    {
        return $request->getHeaders()->has('Authorization') &&
            str_starts_with($request->getHeaders()->get('Authorization'), 'Bearer ');
    }
}