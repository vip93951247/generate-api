<?php

namespace App\Services;

use App\Repositories\GenerateValuesRepository;

class GeneratorService
{
    public const NUMBER_MIN = 1;
    public const NUMBER_MAX = 100000;

    private GenerateValuesRepository $generateValuesRepository;

    public function __construct(GenerateValuesRepository $generateValuesRepository)
    {
        $this->generateValuesRepository = $generateValuesRepository;
    }

    public function getGenerateValueByHash(string $hash)
    {
        return $this->generateValuesRepository->getByHash($hash);
    }

    public function generateValue(): array
    {
        $generateValue = $this->getRandomValue(static::NUMBER_MIN, static::NUMBER_MAX);

        $data = [
            'value' => $generateValue,
            'hash'  => md5($generateValue.time()),
            'created_at' => date('Y-m-d H:i:s')
        ];

        $this->generateValuesRepository->insertGenerateValue($data);

        return $data;
    }

    public function getRandomValue(int $min, int $max): int
    {
        return random_int($min, $max);
    }
}