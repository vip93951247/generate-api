<?php

namespace App\Services;

use App\Repositories\UserRepository;

class AuthService
{
    private UserRepository $userRepo;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    public function doAuth(string $login, string $password)
    {
        $user = $this->userRepo->getOneByData($login, $password);

        $newToken   = $this->generateToken($login, $password);
        $expireDate = $this->getExpireDate(new \DateInterval('PT60S'));

        if ($user === null) {
            $data = [
                'login'     => $login,
                'password'  => $password,
                'token'     => $newToken,
                'token_expire_at' => $expireDate
            ];

            return $this->userRepo->createUser($data);
        }

        $data = [
            'token' => $newToken,
            'token_expire_at' => $expireDate
        ];

        $this->userRepo->updateById($data, $user->id);

        return $newToken;
    }

    public function getExpireDate(\DateInterval $interval): string
    {
        $date = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
        $date->add($interval);
        return $date->format('Y-m-d H:i:s');
    }

    public function retrieveToken(string $token): bool
    {
        return $this->userRepo->getOneByActiveToken($token) !== null;
    }

    public function generateToken(string $login, string $password): string
    {
        return md5($login.$password.time());
    }
}